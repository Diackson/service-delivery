import React from 'react'
import AddOrganisation from '../components/Organisation/AddOrganisation'
import OrganisationList from '../components/Organisation/OrganisationList'
// import "./dashboard.css"

export default function Dashboard() {
  return (
    <div className="bg-gray-300 mx-auto">
      <div className="grid grid-rows-2 gap-8 ">      
        <div className='flex flex-wrap justify-center'>
          <OrganisationList />   
        </div>
        <div className="flex flex-wrap justify-center">
          <AddOrganisation />
        </div>
      </div>
    </div>
    
  ) 
}
