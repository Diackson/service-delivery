import React, {FC} from 'react'
import { useLocation } from 'react-router-dom';
import { ApolloError, gql, useQuery } from '@apollo/client';
import ParentList from '../components/Parent/ParentList';
import ServiceList from '../components/ServiceDelivery/ServiceList';
import EditOrganisation from '../components/Organisation/EditOrganisation';
import AddParent from '../components/Parent/AddParent';
import AddOrder from '../components/Order/AddOrder';

const QUERY_ORGANISATION_Details = gql`
  query OrganisationByName($name: String!) {
    organisationByName(name:$name) {
      id
      name
      description
      address
      parentSet{
        id
        firstName
        lastName
        adress
        organisation{
            id
            name
        }
      }
    }
  }
`;


const Organisation: FC = () => {
    const location = useLocation();
    const {loading, error, data} = useQuery(QUERY_ORGANISATION_Details, {
        variables: { name:location.state.name },
    });
    if(loading) return <p>Loading...</p>
    if(error) return <p>{error.message}</p> 

    return (
        <div className="grid grid-cols-2 gap-2 bg-gray-300 mx-auto">
            <div className="grid grid-rows-2 gap-2">
                <div className="min-w-[350px] max-w-[350px] m-2 py-8 px-8  mx-auto bg-white rounded-xl shadow-md overflow-hidden md:max-w-2xl">
                    <div className="md:flex">
                        <div className="md:shrink-0">
                            <svg className="h-6 w-6 stroke-sky-500" ></svg>
                        </div>
                        <div className="p-8">
                            <div className="uppercase tracking-wide text-sm text-indigo-500 font-semibold">{data.organisationByName.name}</div>
                            <div className="block mt-1 text-lg leading-tight font-medium text-black hover:underline">{data.organisationByName.description}</div>
                            <p className="mt-2 text-slate-500">{data.organisationByName.address}</p>
                        </div>
                        <div className="flex flex-row ">
                            <div className="p-2 block">
                                <AddParent id={data.organisationByName.id} name={data.organisationByName.name} />
                            </div>
                            <div className="p-2 block">
                                <EditOrganisation id={data.organisationByName.id} name={data.organisationByName.name} description={data.organisationByName.description} address={data.organisationByName.address} />
                            </div>
                            <div className="p-2 block">
                                <AddOrder id={data.organisationByName.id} name={data.organisationByName.name} parents={data.organisationByName.parentSet} />
                            </div>
                        </div>                        
                    </div>
                </div>
                <div className="m-2 py-8 px-8 max-w-full mx-auto bg-gray-300 rounded-xl shadow-md overflow-hidden md:max-w-2xl">
                    <h2> Members </h2>
                    <div className="md:flex">
                        <ParentList parents={data.organisationByName.parentSet}  />
                    </div>
                </div>
            </div>
           {/* Left column */}
            <div className="max-w-md mx-auto rounded-xl shadow-md overflow-hidden md:max-w-2xl">
            <h2> Services Ordered </h2>
                <div className="md:flex">
                    <ServiceList id={data.organisationByName.id} />
                </div>
            </div>
        </div>
        
    )
}
export default Organisation;