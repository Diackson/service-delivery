import { gql } from '@apollo/client';


export const QUERY_PARENT_LIST = gql`
  query ParentList {
    parents {
        id
        firstName
        lastName
        email
        adress
        organisation {
          id
          name
        }
    }
  }
`;