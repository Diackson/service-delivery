import React, { FC, useState, useEffect } from 'react';
import Modal from 'react-bootstrap/Modal';
import { gql, useMutation } from '@apollo/client';

const MUTATE_DATA = gql`
  mutation MUTATE_DATA($firstName: String!, $lastName: String!, $email: String, $address: String, $organisation:ID!){
    createParent(parentData:{firstName: $firstName , lastName: $lastName, email: $email, adress: $address, organisation: $organisation}
    ) {
    parent {
      id
      firstName
      lastName
      email
      adress
      organisation{
        id
        name
      }
      
    }
  }
  }
`;
const QUERY_ORGANISATION_Details = gql`
  query OrganisationByName($name: String!) {
    organisationByName(name:$name) {
      id
      name
      description
      address
      parentSet{
        id
        firstName
        lastName
      }
    }
  }
`;

interface Iprops{
    id: number;
    name: string;
    
}
const AddParent: FC<Iprops> = (props) => {
    const [show, setShow] = useState(false);
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [address, setAddress] = useState("");
    const [organisation, setOrganisation] = useState(0);
    const [createParent, { loading, error, data } ]= useMutation(MUTATE_DATA, {
      refetchQueries: [
        { query:QUERY_ORGANISATION_Details },
      ]
    });

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);   

    useEffect(() => {
      if(data){
        console.log(data);
        setFirstName("");
        setLastName("");
        setEmail("");
        setAddress("");
        setOrganisation(0);

      } 
      setShow(false);    
    }, [data]);

    if(loading) return <p>Loading...</p>
    if(error) return <p>{error.message}</p> 
  
    return (
      <>
        <button className="block mx-auto my-auto m-2 bg-purple-600 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded"
                onClick={() => {
                  setOrganisation(props.id)
                  handleShow();
                }}
                >Add Parent</button>                       
  
        <Modal
          show={show}
          onHide={handleClose}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Add Parent</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <form onSubmit={(e:React.FormEvent) => {
            e.preventDefault();
            createParent({ variables: {firstName: firstName, lastName: lastName, email: email, address: address, organisation: props.id}});
          }} 
          id="editOrganisation" 
          className="w-full max-w-sm"
          >
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="firstName">
                firstName
                </label>
                </div>
                <div className="md:w-2/3">
                <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                        id="firstName"
                        type="text" 
                        value={firstName} 
                        onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                            setFirstName(e.target.value)
                        }}
                        />
                </div>
            </div>
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="lastName">
                lastName
                </label>
                </div>
                <div className="md:w-2/3">
                <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" 
                        id="lastName" 
                        type="text" 
                        value={lastName}
                        onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                          setLastName(e.target.value)
                        }}
                        />
                </div>
            </div>
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="email">
                  email
                </label>
                </div>
                <div className="md:w-2/3">
                  <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                        id="email"
                        type="text"
                        value={email}
                        onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                            setEmail(e.target.value)
                        }}
                        />
                </div>
            </div>
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="address">
                    Address
                </label>
                </div>
                <div className="md:w-2/3">
                  <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                        id="address"
                        type="text"
                        value={address}
                        onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                            setAddress(e.target.value)
                        }}
                        />
                </div>
            </div>
          </form>
          {error ? <p> Something went wrong </p>: null}
          </Modal.Body>
          <Modal.Footer>
            <button className="bg-purple-600 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded" form="editOrganisation" >Add</button>
            <button className="bg-slate-400 hover:bg-slate-500 text-white font-bold py-2 px-4 rounded" onClick={handleClose}>Close</button>            
          </Modal.Footer>
        </Modal>
      </>
    );
}
export default AddParent;