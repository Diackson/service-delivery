import React, { FC } from "react";
import EditParent from "./EditParent";

export interface IParent {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    adress:string;
    organisation:{
      id: number;
      name: string;
    };
}

interface Iprop {
  parents: IParent[];
}

const ParentList: FC<Iprop>  = (props) =>{    
    
  return (
    <div className="flex flex-wrap justify-center">
        {props.parents.map((parent: IParent ) => (
          
          <div key={parent.id} className="m-2 py-8 px-8 max-w-sm bg-white rounded-xl shadow-lg space-y-2 sm:py-4 sm:flex sm:items-center sm:space-y-0 sm:space-x-6">
            <img className="object-cover rounded-full h-[100px] w-[100px] block mx-auto sm:mx-0 sm:shrink-0" src="/img/erin-lindford.jpg" alt="Logo"></img>
            <div className="text-center space-y-2 sm:text-left"  >
              <div className="space-y-0.5">
                <p className="text-lg text-black font-semibold">
                  {parent.firstName}            
                </p>
                <p className="text-slate-500 font-medium">
                  {parent.lastName} 
                </p>
                <EditParent id={parent.id} firstName={parent.firstName} lastName={parent.lastName} email={parent.email} address={parent.adress} organisation={parent.organisation.id} />                    
               </div>
            </div>
            </div>
        ))}
    </div>
  )
}

export default ParentList;