import React, { FC, useState, useEffect } from 'react';
import Modal from 'react-bootstrap/Modal';
import { gql, useMutation } from '@apollo/client';

const MUTATE_DATA = gql`
  mutation MUTATE_DATA($description: String!, $location: String!, $provider: String!){
    createService(serviceData:{description: $description,location:$location, provider:$provider}){
      service{
        id
        description
        location
        provider
      }
    }
  }

`;
const QUERY_ORGANISATION_Details = gql`
  query OrganisationByName($name: String!) {
    organisationByName(name:$name) {
      id
      name
      description
      address
      parentSet{
        id
        firstName
        lastName
        adress
        organisation{
            id
            name
        }
      }
    }
  }
`;

const AddService: FC = () => {
    const [show, setShow] = useState(false);
    const [description, setDescription] = useState("");
    const [location, setLocation] = useState("");
    const [provider, setProvider] = useState("");
    const [createService, { loading, error, data } ]= useMutation(MUTATE_DATA, {
      refetchQueries: [
        { query:QUERY_ORGANISATION_Details },
      ]
    });

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);   

    useEffect(() => {
      if(data){
        console.log(data);
        // setDescription("");
        // setLocation("");
        // setProvider("");
      } 
      setShow(false);    
    }, [data]);

    if(loading) return <p>Loading...</p>
    if(error) return <p>{error.message}</p> 
  
    return (
      <>
        <button onClick={handleShow} className="block mx-auto my-auto m-2 bg-purple-600 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded">Add Organisation</button>                       
  
        <Modal
          show={show}
          onHide={handleClose}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Add Organisation</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <form onSubmit={(e:React.FormEvent) => {
            e.preventDefault();
            createService({ variables: {description: description, location: location, provider: provider}});
          }} 
          id="addOrganisation" 
          className="w-full max-w-sm"
          >
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="description">
                    Description
                </label>
                </div>
                <div className="md:w-2/3">
                <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                        id="descritpion"
                        type="text" 
                        value={description} 
                        onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                            setDescription(e.target.value)
                        }}
                        />
                </div>
            </div>
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="location">
                    Location
                </label>
                </div>
                <div className="md:w-2/3">
                <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" 
                        id="location" 
                        type="text" 
                        value={location}
                        onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                            setLocation(e.target.value)
                        }}
                        />
                </div>
            </div>
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="provider">
                    Provider
                </label>
                </div>
                <div className="md:w-2/3">
                <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                        id="provider"
                        type="text"
                        value={provider}
                        onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                            setProvider(e.target.value)
                        }}
                        />
                </div>
            </div>
          </form>
          {error ? <p> Something went wrong </p>: null}
          </Modal.Body>
          <Modal.Footer>
            <button className="bg-purple-600 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded" form="addOrganisation" >Add</button>
            <button className="bg-slate-400 hover:bg-slate-500 text-white font-bold py-2 px-4 rounded" onClick={handleClose}>Close</button>            
          </Modal.Footer>
        </Modal>
      </>
    );
}
export default AddService;