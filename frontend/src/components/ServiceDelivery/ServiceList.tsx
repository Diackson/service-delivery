import React, { FC } from 'react';
import { gql, useQuery } from '@apollo/client';
import EditService from './EditService';


const QUERY_SERVICE_LIST = gql`
  query ServiceList {
    ServiceDelivery{
        id
        provider
        location
        orderSet{
          id
          orderDate
          organisation{
            id
            name
          }   
        }
    }
  }
`;

export interface IService {
    id: number;
    description: string;
    provider: string;
    location: string;
    orderSet:[
        {
            id: number;
            orgabisation: number;
        }
    ]
}
interface IProps {
    id: number;
}

const ServiceList: FC<IProps> = (props) => {
    const {loading, error, data} = useQuery(QUERY_SERVICE_LIST)
    if(loading) return <p>Loading...</p>
    if(error) return <p>{error.message}</p> 
    const ordered = data.ServiceDelivery.filter((o:IService) => o.orderSet.filter((x) => x.id === props.id) ) 
    return ( 
        <div className="flex flex-wrap justify-center">
            {ordered.map((service: IService ) => (
                <div key={service.id} className="m-2 py-8 px-8 max-w-sm bg-white rounded-xl shadow-lg space-y-2 sm:py-4 sm:flex sm:items-center sm:space-y-0 sm:space-x-6">
                <img className="object-cover rounded-full h-[100px] w-[100px] block mx-auto sm:mx-0 sm:shrink-0" src="/img/erin-lindford.jpg" alt="Logo"></img>
                <div className="text-center space-y-2 sm:text-left"  >
                    <div className="space-y-0.5">
                    <p className="text-lg text-black font-semibold">
                        {service.description}            
                    </p>
                    <p className="text-slate-500 font-medium">
                        {service.provider} 
                    </p>
                    <p className="text-slate-500 font-medium">
                        {service.location} 
                    </p>
                    <EditService id={service.id} description={service.description} location={service.location} provider={service.provider} />                   
                    </div>
                </div>
                </div>
            ))}
        </div>       
        
    );
}
export default ServiceList;