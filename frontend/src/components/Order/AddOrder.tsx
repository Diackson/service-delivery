import React, { FC, useState, useEffect } from 'react';
import Modal from 'react-bootstrap/Modal';
import { gql, useMutation, useQuery } from '@apollo/client';
import { IParent } from '../Parent/ParentList';
import { IService } from '../ServiceDelivery/ServiceList';

const MUTATE_DATA = gql`
  mutation MUTATE_DATA($organisation: ID , $parent: ID, $service: ID){
    createOrder(orderData:{organisation:$organisation,parent:$parent,service:$service}){
        order{
          id
          organisation{
            id
            name
          }
        }
      }
  }

`;
const QUERY_SERVICE_LIST = gql`
  query ServiceList {
    ServiceDelivery {
        id
        description
        provider
        location
    }
  }
`;

interface Iprops{
    id: number;
    name: string;
    parents: IParent[];
}
const AddOrder: FC<Iprops> = (props) => {
    const [show, setShow] = useState(false);
    const [organisation, setOrganisation] = useState(props.id);
    const [parent, setParent] = useState("");
    const [service, setService] = useState("");
    const {loading, error, data} = useQuery(QUERY_SERVICE_LIST);
    
    const [createOrder, { loading:createOrderLoading, error: createOrderError, data: createOrderData } ]= useMutation(MUTATE_DATA);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);   

    useEffect(() => {
      if(createOrderData){
        console.log(createOrderData);
        // setOrganisation(0);
        // setParent(0);
        // setService(0);
      } 
      setShow(false);    
    }, [createOrderData]);

    if(loading) return <p>Loading...</p>
    if(error) return <p>{error.message}</p> 
    if(createOrderLoading) return <p>Loading...</p>
    if(createOrderError) return <p>{createOrderError.message}</p> 
    
    console.log( parent)
    console.log(service)
    return (
      <>
        <button onClick={handleShow} className="block mx-auto my-auto m-2 bg-purple-600 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded">Order</button>                       
  
        <Modal
          show={show}
          onHide={handleClose}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Order A Service</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <form onSubmit={(e:React.FormEvent) => {
            e.preventDefault();
            createOrder({ variables: {organisation: organisation, parent: Number(parent), service: Number(service) }});
          }} 
          id="editOrganisation" 
          className="w-full max-w-sm"
          >
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="parents">
                    Parents
                </label>
                </div>
                <div className="md:w-2/3 inline-block relative">
                    <select id="parents" className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline"
                            value={parent}
                            onChange= {
                                (e: React.ChangeEvent<HTMLSelectElement>) => {
                                    setParent(e.target.value)
                                }
                            }
                    >
                        <option value=""></option>
                        {
                            props.parents.map((option) => (
                                <option value={option.id}>{option.firstName}  {option.lastName}</option>
                            ))
                        }
                    </select>
                    <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                        <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                    </div>
                </div>
            </div>          
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="service">
                    Services
                </label>
                </div>
                <div className="md:w-2/3 inline-block relative">
                    <select id="service" className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline"
                        value={service}
                        onChange= {
                            (e: React.ChangeEvent<HTMLSelectElement>) => {
                                setService(e.target.value);
                            }
                        }>
                            <option value=""></option>
                        {
                             data.ServiceDelivery.map((option: IService) => (
                                <option value={option.id}>{option.description} </option>
                            ))
                        }
                    </select>
                    <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                        <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                    </div>
                </div>
            </div>
          </form>
          {error ? <p> Something went wrong </p>: null}
          </Modal.Body>
          <Modal.Footer>
            <button className="bg-purple-600 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded" form="editOrganisation" >Add</button>
            <button className="bg-slate-400 hover:bg-slate-500 text-white font-bold py-2 px-4 rounded" onClick={handleClose}>Close</button>            
          </Modal.Footer>
        </Modal>
      </>
    );
}
export default AddOrder;