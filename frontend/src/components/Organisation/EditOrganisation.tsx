import React, { FC, useState, useEffect } from 'react';
import Modal from 'react-bootstrap/Modal';
import { gql, useMutation } from '@apollo/client';

const MUTATE_DATA = gql`
  mutation MUTATE_DATA($name: String!, $description: String, $address: String, $id: ID){
    updateOrganisation(organisationData:{id: $id, name: $name,description:$description, address:$address}){
      organisation{
        id
        name
        address
      }
    }
  }

`;
const QUERY_ORGANISATION_Details = gql`
  query OrganisationByName($name: String!) {
    organisationByName(name:$name) {
      id
      name
      description
      address
      parentSet{
        id
        firstName
        lastName
        adress
        organisation{
            id
            name
        }
      }
    }
  }
`;



interface Iprops{
    id:number;
    name: string;
    description: string;
    address: string;
}
const EditOrganisation: FC<Iprops> = (props) => {
    const [show, setShow] = useState(false);
    const [id, setId] = useState(props.id);
    const [name, setName] = useState(props.name);
    const [description, setDescription] = useState(props.description);
    const [address, setAddress] = useState(props.address);
    const [editOrganisation, {loading, error, data }] = useMutation(MUTATE_DATA, {
      refetchQueries: [
        {
          query: QUERY_ORGANISATION_Details
        },
      ]
    });
    useEffect(() => {
      if(data){
        console.log(data);
      } 
      setShow(false);    
    }, [data]);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    if(loading) return <p>Loading...</p>
    if(error) return <p>{error.message}</p> 
  
    return (
      <>
        <button onClick={handleShow} className="block mx-auto my-auto m-2 bg-purple-600 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded">Edit</button>                       
  
        <Modal
          show={show}
          onHide={handleClose}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Edit Organisation</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <form id="editOrganisation" className="w-full max-w-sm"
                onSubmit={
                  (e:React.FormEvent) => {
                    e.preventDefault();
                    editOrganisation({variables: {id:id, name: name, description: description, address: address}});
                  }
                }
            >
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="name">
                    Name
                </label>
                </div>
                <div className="md:w-2/3">
                <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                        id="name"
                        type="text" 
                        value={name} 
                        onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                            setName(e.target.value)
                        }}
                        />
                </div>
            </div>
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="description">
                    Description
                </label>
                </div>
                <div className="md:w-2/3">
                <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" 
                        id="description" 
                        type="text" 
                        value={description}
                        onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                            setDescription(e.target.value)
                        }}
                        />
                </div>
            </div>
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="address">
                    Address
                </label>
                </div>
                <div className="md:w-2/3">
                <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                        id="address"
                        type="text"
                        value={address}
                        onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                            setAddress(e.target.value)
                        }}
                        />
                </div>
            </div>
            </form>
          </Modal.Body>
          <Modal.Footer>
            <button className="bg-purple-600 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded" form="editOrganisation" >Update</button>
            <button className="bg-slate-400 hover:bg-slate-500 text-white font-bold py-2 px-4 rounded" onClick={handleClose}>Close</button>            
          </Modal.Footer>
        </Modal>
      </>
    );
}
export default EditOrganisation;