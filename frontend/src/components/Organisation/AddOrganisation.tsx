import React, { FC, useState, useEffect } from 'react';
import Modal from 'react-bootstrap/Modal';
import { gql, useMutation } from '@apollo/client';

const MUTATE_DATA = gql`
  mutation MUTATE_DATA($name: String!, $description: String!, $address: String!){
    createOrganisation(organisationData:{name: $name,description:$description, address:$address}){
      organisation{
        id
        name
        address
      }
    }
  }

`;
const QUERY_ORGANISATION_LIST = gql`
  query OrganisationList {
    organisations {
      id
      name
      description
      address
    }
  }
`;

// interface Iprops{
//     name: string;
//     description: string;
//     address: string;
// }
const AddOrganisation: FC = () => {
    const [show, setShow] = useState(false);
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [address, setAddress] = useState("");
    const [createOrganisation, { loading, error, data } ]= useMutation(MUTATE_DATA, {
      refetchQueries: [
        { query:QUERY_ORGANISATION_LIST },
      ]
    });

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);   

    useEffect(() => {
      if(data){
        console.log(data);
        setName("");
        setDescription("");
        setAddress("");
      } 
      setShow(false);    
    }, [data]);

    if(loading) return <p>Loading...</p>
    if(error) return <p>{error.message}</p> 
  
    return (
      <>
        <button onClick={handleShow} className="block mx-auto my-auto m-2 bg-purple-600 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded">Add Organisation</button>                       
  
        <Modal
          show={show}
          onHide={handleClose}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Add Organisation</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <form onSubmit={(e:React.FormEvent) => {
            e.preventDefault();
            createOrganisation({ variables: {name: name, description: description, address: address}});
          }} 
          id="editOrganisation" 
          className="w-full max-w-sm"
          >
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="name">
                    Name
                </label>
                </div>
                <div className="md:w-2/3">
                <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                        id="name"
                        type="text" 
                        value={name} 
                        onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                            setName(e.target.value)
                        }}
                        />
                </div>
            </div>
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="description">
                    Description
                </label>
                </div>
                <div className="md:w-2/3">
                <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" 
                        id="description" 
                        type="text" 
                        value={description}
                        onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                            setDescription(e.target.value)
                        }}
                        />
                </div>
            </div>
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="address">
                    Address
                </label>
                </div>
                <div className="md:w-2/3">
                <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                        id="address"
                        type="text"
                        value={address}
                        onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                            setAddress(e.target.value)
                        }}
                        />
                </div>
            </div>
          </form>
          {error ? <p> Something went wrong </p>: null}
          </Modal.Body>
          <Modal.Footer>
            <button className="bg-purple-600 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded" form="editOrganisation" >Add</button>
            <button className="bg-slate-400 hover:bg-slate-500 text-white font-bold py-2 px-4 rounded" onClick={handleClose}>Close</button>            
          </Modal.Footer>
        </Modal>
      </>
    );
}
export default AddOrganisation;