import React, { FC } from 'react';
import { gql, useQuery } from '@apollo/client';
import { useNavigate, NavigateFunction } from 'react-router-dom';

const QUERY_ORGANISATION_LIST = gql`
  query OrganisationList {
    organisations {
      id
      name
      description
      address
    }
  }
`;
export interface IOrganisation {
    id:number;
    name: String;
    description: String;
    address: String;
    parentSet: [];
}

const OrganisationList: FC  = () => {

  const navigate: NavigateFunction = useNavigate();
  const {loading, error, data} = useQuery(QUERY_ORGANISATION_LIST)
  if(loading) return <p>Loading...</p>
  if(error) return <p>{error.message}</p> 

  const goToDetails= (name:String)=>(event: React.MouseEvent) => {
    navigate("/organisation", {
      state: {
        name:name
      },
      replace: true
  })

  }
  return (
    <>        
      {data.organisations.map((organisation: IOrganisation ) => (
        <div key={organisation.id} className="min-w-[350px] max-w-[350px] m-2 py-8 px-8  bg-white rounded-xl shadow-lg space-y-2 sm:py-4 sm:flex sm:items-center sm:space-y-0 sm:space-x-6">
          <img className="object-cover rounded-full h-[100px] w-[100px] block mx-auto  sm:mx-0 sm:shrink-0" src="/img/erin-lindford.jpg" alt="Logo"></img>
          <div className="text-center space-y-2 sm:text-left"  >
            <div className="space-y-0.5">
              <p className="text-lg text-black font-semibold">
                {organisation.name}            
              </p>
              <p className="text-slate-500 font-medium">
                {organisation.address} 
              </p>
              <button onClick={goToDetails(organisation.name)} className="px-4 py-1 text-sm text-purple-600 font-semibold rounded-full border border-purple-200 hover:text-white hover:bg-purple-600 hover:border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:ring-offset-2">Details</button>                       
             </div>
          </div>
          </div>
        ))}
    </>
    
  )
  
}
export default OrganisationList;
 