import React, { FC } from 'react';
import './App.css';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Dashboard from './pages/Dashboard';
import Organisation from './pages/Organisation';


const client = new ApolloClient({
  uri: 'http://localhost:8000/graphql/',
  cache: new InMemoryCache(),

});

const  App: FC = () => {
  return (
    <ApolloProvider client={client}>
      <Router>
        <Routes>
          <Route path='/' element={<Dashboard />} />
          <Route path='/organisation' element={<Organisation />} />
        </Routes>
      </Router>
    </ApolloProvider>
    
  );
}

export default App;
