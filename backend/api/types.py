from graphene_django import DjangoObjectType
from .models import Organisation, Parent, Order, ServiceDelivery

class OrganisationType(DjangoObjectType):
    class Meta:
        model = Organisation
        fields = '__all__'

class ParentType(DjangoObjectType):
    class Meta:
        model = Parent
        fields = '__all__'

class OrderType(DjangoObjectType):
    class Meta:
        model = Order
        fields = '__all__'
        
class ServiceDeliveryType(DjangoObjectType):
    class Meta:
        model = ServiceDelivery
        fields = '__all__'