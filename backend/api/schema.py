import graphene
from .types import OrganisationType, ParentType, OrderType, ServiceDeliveryType
from .models import Organisation, Parent, Order, ServiceDelivery
from .mutations import CreateOrganisation, UpdateOrganisation, DeleteOrganisation, CreateParent, UpdateParent, DeleteParent, CreateService, UpdateService, DeleteService, CreateOrder

class Query(graphene.ObjectType):
    organisations = graphene.List(OrganisationType)
    parents = graphene.List(ParentType)
    orders = graphene.List(OrderType)
    ServiceDelivery = graphene.List(ServiceDeliveryType)
    oragnisation_member = graphene.List(ParentType, name=graphene.String(required=True))
    organisation_by_name = graphene.Field(OrganisationType, name=graphene.String(required=True))

    def resolve_organisations(self, info):
        return Organisation.objects.all()

    def resolve_parents(self, info):
        return Parent.objects.select_related('organisation').all()
        
    def resolve_orders(self, info):
        return Order.objects.select_related('organisation').all()

    def resolve_ServiceDelivery(self, info):
        return ServiceDelivery.objects.all()

    def resolve_organisation_member(self, info, name):
        try:
            return Parent.objects.filter(organisation=Organisation.objects.get(name=name))
        except Parent.DoesNotExist:
            return None

    def resolve_organisation_by_name(self, info, name):
        try:
            return Organisation.objects.get(name=name)
        except Organisation.DoesNotExist:
            return None     
class Mutations(graphene.ObjectType):
    create_parent = CreateParent.Field()
    update_parent = UpdateParent.Field()
    delete_parent = DeleteParent.Field()
    create_organisation = CreateOrganisation.Field()
    update_organisation = UpdateOrganisation.Field()
    delete_organisation = DeleteOrganisation.Field()
    create_service = CreateService.Field()
    update_service = UpdateService.Field()
    delete_service = DeleteService.Field()
    create_order = CreateOrder.Field()
    

schema = graphene.Schema(query=Query, mutation=Mutations)
    
