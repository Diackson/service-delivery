from django.db import models

# Create your models here.

class ServiceDelivery(models.Model):
    description = models.CharField(max_length=100)
    provider = models.CharField(max_length=100)
    location = models.CharField(max_length=100)

    def __str__(self):
        return self.description

class Organisation(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500)
    address = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Parent(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField()
    adress = models.CharField(max_length=100)
    organisation = models.ForeignKey(Organisation, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)

class Order(models.Model):
    organisation = models.ForeignKey(Organisation, on_delete=models.CASCADE, null=True)
    parent = models.ForeignKey(Parent, on_delete=models.CASCADE, null=True)
    service = models.ForeignKey(ServiceDelivery, on_delete=models.CASCADE, null=True)
    order_date = models.DateField(auto_now=True)

