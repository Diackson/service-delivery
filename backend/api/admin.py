from django.contrib import admin
from .models import Parent, ServiceDelivery, Organisation, Order

# Register your models here.
admin.site.register(Parent)
admin.site.register(ServiceDelivery)
admin.site.register(Organisation)
admin.site.register(Order)
