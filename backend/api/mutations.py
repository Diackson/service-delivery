import graphene
from .inputs import OrganisationInput, ParentInput, OrderInput, ServiceDeliveryInput
from .models import Organisation, Parent, Order, ServiceDelivery
from .types import OrganisationType, ParentType, OrderType, ServiceDeliveryType

class CreateOrganisation(graphene.Mutation):
    class Arguments:
        organisation_data = OrganisationInput(required=True)

    organisation = graphene.Field(OrganisationType)

    def mutate(self, info, organisation_data=None):
        organisation_instance = Organisation(name=organisation_data.name, description=organisation_data.description, address=organisation_data.address)
        organisation_instance.save()
        return CreateOrganisation(organisation=organisation_instance)

class UpdateOrganisation(graphene.Mutation):
    class Arguments:
        organisation_data = OrganisationInput(required=True)

    organisation = graphene.Field(OrganisationType)

    def mutate(self, info, organisation_data=None):
        organisation_instance = Organisation.objects.get(pk=organisation_data.id)

        if organisation_instance:
            organisation_instance.name = organisation_data.name
            organisation_instance.description = organisation_data.description
            organisation_instance.address = organisation_data.address
            organisation_instance.save()
            return UpdateOrganisation(organisation=organisation_instance)
        return UpdateOrganisation(organisation=None)

class DeleteOrganisation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()
    organisation = graphene.Field(OrganisationType)

    def mutate(self, info, id):
        organisation_instance = Organisation.objects.get(pk=id)
        organisation_instance.delete()
        return None

class CreateService(graphene.Mutation):
    class Arguments:
        service_data = ServiceDeliveryInput(required=True)

    service = graphene.Field(ServiceDeliveryType)

    def mutate(self, info, service_data=None):
        service_instance = ServiceDelivery(description=service_data.description, provider=service_data.provider, location=service_data.location)
        service_instance.save()
        return CreateService(service=service_instance)

class UpdateService(graphene.Mutation):
    class Arguments:
        service_data = ServiceDeliveryInput(required=True)

    service = graphene.Field(ServiceDeliveryType)

    def mutate(self,info, service_data=None):
        service_instance = ServiceDelivery.objects.get(pk=service_data.id)
        if service_instance:
            service_instance.description = service_data.description
            service_instance.provider = service_data.provider
            service_instance.location = service_data.location
            service_instance.save()
            return UpdateService(service=service_instance)
        return UpdateService(service=None)

class DeleteService(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    service = graphene.Field(ServiceDeliveryType)
    
    def mutate(self, info, id):
        service_instance = ServiceDelivery.objects.get(pk=id)
        service_instance.delete()
        return None 

class CreateOrder(graphene.Mutation):
    class Arguments:
        order_data = OrderInput(required=True)

    order = graphene.Field(OrderType)

    def mutate(self, info, order_data=None):
        organisation_instance = Organisation.objects.get(pk=order_data.organisation)
        parent_instance = Parent.objects.get(pk=order_data.parent)
        service_instance = ServiceDelivery.objects.get(pk=order_data.service)
        order_instance = Order(organisation=organisation_instance, parent=parent_instance, service=service_instance)
        order_instance.save()
        return CreateOrder(order=order_instance)

class UpdateOrder(graphene.Mutation):
    class Arguments:
        order_data = OrderInput(required=True)

    order = graphene.Field(OrderType)

    def mutate(self, info, order_data=None):
        order_instance = Order.objects.get(pk=order_data.id)        
        if order_instance:
            order_instance.organisation = Organisation.objects.get(pk=order_data.organisation)
            order_instance.parent = Parent.objects.get(pk=order_data.parent) 
            order_instance.service = ServiceDelivery.objects.get(pk=order_data.service)
            order_instance.order_date = order_data.order_date
            order_instance.save()
            return UpdateOrder(order=order_instance)
        return UpdateOrder(order=order_instance)

class DeleteOrder(graphene.Mutation):
    class Arguments:
        id = graphene.ID()
    
    order = graphene.Field(OrderType)

    def mutate(self, info, id):
        order_instance = Order.objects.get(pk=id)
        order_instance.delete()
        return None

class CreateParent(graphene.Mutation):
    class Arguments:
        parent_data = ParentInput(required=True)

    parent = graphene.Field(ParentType)

    def mutate(self, info, parent_data=None):
        organisation_instance = Organisation.objects.get(pk=parent_data.organisation)
        parent_instance = Parent(first_name=parent_data.first_name, last_name=parent_data.last_name, email=parent_data.email, adress=parent_data.adress,organisation=organisation_instance)
        parent_instance.save()
        return CreateParent(parent=parent_instance)

class UpdateParent(graphene.Mutation):
    class Arguments:
        parent_data = ParentInput(required=True)

    parent = graphene.Field(ParentType)
    
    def mutate(self, info, parent_data=None):
        parent_instance = Parent.objects.get(pk=parent_data.id) 
        organisation_instance = Organisation.objects.get(pk=parent_data.organisation)
        print("DEBUG:", parent_instance.organisation)
        if parent_instance:
            parent_instance.first_name = parent_data.first_name
            parent_instance.last_name = parent_data.last_name
            parent_instance.email = parent_data.email
            parent_instance.adress = parent_data.adress
            parent_instance.organisation = organisation_instance
            parent_instance.save()
            return UpdateParent(parent=parent_instance)    
        return UpdateParent(parent=None)

class DeleteParent(graphene.Mutation):
    class Arguments:
        id = graphene.ID()
    
    parent = graphene.Field(ParentType)

    def mutate(self, info, id):
        parent_instance = Parent.objects.get(pk=id)
        parent_instance.delete()
        return None