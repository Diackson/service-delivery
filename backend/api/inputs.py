import graphene

class OrganisationInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)
    description = graphene.String()
    address = graphene.String()

class ParentInput(graphene.InputObjectType):
    id = graphene.ID()
    first_name = graphene.String(required=True)
    last_name = graphene.String(required=True)
    email = graphene.String()
    adress = graphene.String()
    organisation = graphene.ID()

class ServiceDeliveryInput(graphene.InputObjectType):
    id = graphene.ID()
    description = graphene.String(required=True)
    provider = graphene.String()
    location = graphene.String()

class OrderInput(graphene.InputObjectType):
    id = graphene.ID()
    organisation = graphene.ID()
    parent = graphene.ID()
    service = graphene.ID()
    order_date = graphene.Date()